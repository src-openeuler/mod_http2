%{!?_httpd_mmn: %global _httpd_mmn %(cat %{_includedir}/httpd/.mmn 2>/dev/null || echo 0-0)}

Name:           mod_http2
Version:        2.0.25
Release:        3
Summary:        Support for the HTTP/2 transport layer
License:        ASL 2.0
URL:            https://icing.github.io/mod_h2/
Source0:        https://github.com/icing/mod_h2/releases/download/v%{version}/%{name}-%{version}.tar.gz

Patch0:         backport-CVE-2024-27316.patch
Patch1:         backport-CVE-2024-36387.patch

BuildRequires:  make gcc pkgconfig httpd-devel >= 2.4.20 libnghttp2-devel >= 1.7.0 openssl-devel >= 1.0.2 autoconf libtool hostname
Requires:       httpd-mmn = %{_httpd_mmn}

%description
The mod_h2 Apache httpd module implements the HTTP2 protocol (h2+h2c) on
top of libnghttp2 for httpd 2.4 servers.

%package_help

%prep
%autosetup -p1

%build
autoreconf -i
%configure --with-apxs=%{_httpd_apxs}
%make_build

%install
%make_install
mkdir -p %{buildroot}%{_httpd_modconfdir}
echo "LoadModule http2_module modules/mod_http2.so" > %{buildroot}%{_httpd_modconfdir}/10-h2.conf
echo "LoadModule proxy_http2_module modules/mod_proxy_http2.so" > %{buildroot}%{_httpd_modconfdir}/10-proxy_h2.conf

%check
make check

%files
%defattr(-,root,root)
%doc AUTHORS
%license LICENSE
%{_httpd_moddir}/*.so
%config(noreplace) %{_httpd_modconfdir}/*.conf

%files help
%defattr(-,root,root)
%doc ChangeLog README README.md

%exclude /etc/httpd/share/doc/*

%changelog
* Mon Jul 08 2024 zhangxianting <zhangxianting@uniontech.com> - 2.0.25-3
- Type:cves
- CVE:CVE-2024-36387
- SUG:NA
- DESC:fix CVE-2024-36387

* Sun Apr 07 2024 gaihuiying <eaglegai@163.com> - 2.0.25-2
- Type:cves
- CVE:CVE-2024-27316
- SUG:NA
- DESC:fix CVE-2024-27316

* Tue Dec 26 2023 gaihuiying <eaglegai@163.com> - 2.0.25-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC: update mod_http2 to 2.0.25

* Fri Jul 21 2023 gaihuiying <eaglegai@163.com> - 2.0.20-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC: update mod_http2 to 2.0.20

* Mon Feb 20 2023 li-long315 <lilong@kylinos.cn> - 2.0.11-1
- Type:Update
- ID:NA
- SUG:NA
- DESC: update mod_http2 to 2.0.11

* Thu Nov 10 2022 gaihuiying <eaglegai@163.com> - 2.0.3-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC: update mod_http2 to 2.0.3

* Wed Oct 19 2022 gaihuiying <eaglegai@163.com> - 1.15.25-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:modify description about mod_http2

* Sat Mar 19 2022 quanhongfei <quanhongfei@h-partners.com> - 1.15.25-1
- Type:requirements
- ID:NA
- SUG:NA
- DESC: update mod_http2 to 1.15.25

* Wed Jan 27 2021 xihaochen <xihaochen@huawei.com> - 1.15.16-1
- Type:requirements
- ID:NA
- SUG:NA
- DESC: update mod_http2 to 1.15.16

* Fri Jul 17 2020 cuibaobao <cuibaobao1@huawei.com> - 1.15.13-2
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update mod_http2 to 1.15.13-2

* Tue Jun 23 2020 gaihuiying <gaihuiying1@huawei.com> - 1.15.11-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update mod_http2 to 1.15.11-1

* Thu Sep 05 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.10.20-4
- Type:enhance
- ID:NA
- SUG:NA
- DESC:new rule

* Mon Aug 12 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.10.20-3
- Package init
